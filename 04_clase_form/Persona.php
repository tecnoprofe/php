<?php
class Persona{            

    public $nombre=null;
    public $apellidos=null;
    public $edad=null;
    
    public function __construct($nombre,$apellidos,$edad)
    {
        $this->nombre=$nombre;
        $this->apellidos=$apellidos;
        $this->edad=$edad;
    }
    public function mostrar_nombre_completo(){        
        $nombre_comp=$this->nombre.", ".$this->apellidos;
        return $nombre_comp;
    }        
}
?>